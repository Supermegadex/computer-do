import { State } from "./Interpreter";
import { Block, Command } from "./parse";
import { compare, getVar, getStringData, setVar } from "./utils";
import { Token } from "./lexer";

export default async function execute(state: State) {
  const block = state.stack[state.stack.length - 1];
  await runBlock([...block.lines], state);
  if (state.isModule) {
    return state.exports;
  }
  process.exit(0);
}

export async function runBlock(lines: (Command | Block)[], state: State) {
  if (lines.length <= 0) {
    return;
  }
  else {
    const line = lines.shift();
    if (line instanceof Command) {
      await runCommand(line, state);
    }
    else if (line instanceof Block) {
      if (line.type === "loop") {
        if (line.args) {
          for (let i = 0; i < line.args[0].value; i++) {
            state.stack.push(line);
            await runBlock([...line.lines], state);
            state.stack.pop();
          }
        }
      }
      else if (line.type === "conditional") {
        const c = compare(state, line.args![0], line.args![1], line.args![2]);
        if (c) {
          state.stack.push(line);
          await runBlock([...line.lines], state);
          state.stack.pop();
        }
      }
      else if (line.type === "function") {
        const fname = line.args!.shift()!.value;
        state.namespaces[line.ns!].DO[fname] = async (state: State, ...args: Token[]) => {
          let i = 0;
          let foundTo = false;
          for (let arg of args) {
            if (arg.type === "Assignment") {
              if (arg.value === "to") {
                foundTo = true;
              }
            }
            else if (!foundTo) {
              line.variables[line.args![i].value] = arg.type === "Identifier" ? getVar(arg.value.toString(), state) : arg.value;
              i++;
            }
          }
          if (foundTo) {
            line.returnTo = args[args.length - 1];
          }
          state.stack.push(line);
          await runBlock([...line.lines], state);
          let returnValue;
          if (line.returnToken) {
            // console.log("RETURN TOKEN: ", line.returnToken);
            returnValue = line.returnToken.type === "Identifier"
                ? getVar(line.returnToken.value.toString(), state)
                : line.returnToken.type === "String"
                  ? getStringData(line.returnToken!, state)
                  : line.returnToken.value;
          }
          state.stack.pop();
          if (line.returnTo) {
            setVar(line.returnTo.value.toString(), returnValue, state);
          }
        }
      }
    }
    await runBlock(lines, state);
  }
}

export async function runCommand(command: Command, state: State) {
  if (command.namespace) {
    const namespace = state.namespaces[command.namespace];
    if (namespace.DO) {
      const method = namespace.DO[command.method];
      if (method) {
        await method(state, ...command.args);
      }
    }
  }
}
