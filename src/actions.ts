import Interpreter, { Namespaces, State } from "./Interpreter";
import { Token } from "./lexer";
import { doArithmetic, getStringData, setVar, getVar, getNumberData, check, extractValue } from "./utils";
import * as fs from 'fs';
import * as readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

export default {
  async print(state: State, message: Token) {
    console.log(getStringData(message, state));
  },
  async set(state: State, identifier: Token, value: Token) {
    if (!value) {
      console.error(`ERROR: Identifier ${identifier.value} is left undefined.`);
      return;
    }
    if (value.type === "Identifier") {
      setVar(identifier.value.toString(), getVar(value.value.toString(), state), state);
    }
    if (value.type === "String") {
      setVar(identifier.value.toString(), getStringData(value, state), state);
    }
    if (value.type === "Number") {
      setVar(identifier.value.toString(), getNumberData(value, state), state);
    }
  },
  ask(state: State, question: Token, to: Token, identifier: Token) {
    return new Promise((resolve, reject) => {
      rl.question(question.value.toString(), answer => {
        setVar(identifier.value.toString(), answer, state);
        resolve();
      });
    });
  },
  askNum(state: State, question: Token, to: Token, identifier: Token) {
    return new Promise((resolve, reject) => {
      const request = (q: string) => { 
        rl.question(q, async (answer) => {
          const num = parseFloat(answer);
          if (isNaN(num)) {
            await request("Please enter a number: ");
          }
          setVar(identifier.value.toString(), num, state);
          state.pause = false;
          resolve();
        });
      };
      request(question.value.toString());
    });
  },
  async add(state: State, ...args: Token[]) {
    doArithmetic(state, "add", ...args);
  },
  async subtract(state: State, ...args: Token[]) {
    doArithmetic(state, "subtract", ...args);
  },
  async multiply(state: State, ...args: Token[]) {
    doArithmetic(state, "multiply", ...args);
  },
  async divide(state: State, ...args: Token[]) {
    doArithmetic(state, "divide", ...args);
  },
  async mod(state: State, ...args: Token[]) {
    doArithmetic(state, "mod", ...args);
  },
  async pow(state: State, ...args: Token[]) {
    doArithmetic(state, "pow", ...args);
  },
  async randomInt(state: State, between: Token, minT: Token, and: Token, maxT: Token, to: Token, variable: Token) {
    if (check(between, "Assignment", "between")) {
      if (check(minT, "Number")) {
        if (check(and, "Assignment", "and")) {
          if (check(maxT, "Number")) {
            if (check(to, "Assignment", "to")) {
              if (check(variable, "Identifier")) {
                const min = (<number>minT.value);
                const max = (<number>maxT.value);
                const val = Math.floor(Math.random() * (max - min + 1)) + min;
                setVar(variable.value.toString(), val, state);
              }
            }
          }
        }
      }
    }
  },
  async ignore(state: State) {
    // do nothing. it's a comment
  },
  async namespace(state: State, ns: Token) {
    if (!state.namespaces.hasOwnProperty(ns.value)) {
      state.namespaces[ns.value] = {
        DO: {}
      }
    }
  },
  async list(state: State, name: Token) {
    setVar(name.value.toString(), [], state);
  },
  async copyList(state: State, list: Token, to: Token, newList: Token) {
    if (check(list, "Identifier")) {
      const l = getVar(list.value.toString(), state);
      if (l instanceof Array) {
        if (check(to, "Assignment", "to")) {
          if (check(newList, "Identifier")) {
            setVar(newList.value.toString(), [...l], state);
          }
        }
      }
    }
  },
  async getItem(state: State, ...args: Token[]) {
    if (args.length >= 4) {
      const from = args.shift()!,
      list = args.shift()!,
      variable = args.pop()!,
      to = args.pop()!,
      indices = [...args];
      // console.log(`${from.value} $${list.value} ${indices.map(d => d.value).join(", ")} ${to.value}, $${variable.value}`);
      if (check(from, "Assignment", "from")) {
        if (check(list, "Identifier")) {
          const l = getVar(list.value.toString(), state);
          // const tail = extractValue(indices.pop()!, state);
          if (l instanceof Array) {
            let value = l;
            indices.forEach(index => {
              if (check(index, "Identifier")) {
                // console.log(`Index: ${index.value}, variable: ${variable.value}, value: ${getVar(index.value.toString(), state)}, list: ${list.value}, stack: ${state.stack.map(d => d.type).join(', ')}`)
                value = value[getVar(index.value.toString(), state)];
              }
              else if (check(index, "String")) {
                value = value[getStringData(index, state)];
              }
              else {
                value = value[(<number>index.value)];
              }
            });
            if (check(variable, "Identifier")) {
              setVar(variable.value.toString(), value, state);
            }
          }
        }
      }
    }
  },
  async setItem(state: State, ...args: Token[]) {
    const list = args.shift()!,
      variable = args.pop()!,
      to = args.pop()!,
      indices = [...args];
    if (list.type === "Identifier") {
      let l = getVar(list.value.toString(), state);
      if (l instanceof Array) {
        const tail = extractValue(indices.pop()!, state);
        indices.forEach(index => {
          if (check(index, "Identifier")) {
            l = l[getVar(index.value.toString(), state)];
          }
          else if (check(index, "String")) {
            l = l[getStringData(index, state)];
          }
          else {
            l = l[(<number>index.value)];
          }
        });
        l[tail] = variable.type === "Identifier"
          ? getVar(variable.value.toString(), state) 
          : variable.type === "String"
            ? getStringData(variable, state)
            : variable.value;
        }
        // if (index.type === "Identifier" || index.type === "Number") {
        //   if (check(to, "Assignment", "to")) {
        //     if (variable.type === "Identifier" || variable.type === "String" || variable.type === "Number") {
        //       l[getNumberData(index, state)] = 
        //   }
        // }
    }
  },
  async push(state: State, ...tokens: Token[]) {
    let newData = [];
    let reachedTo = false;
    for (let token of tokens) {
      if (!reachedTo) {
        if (token.type === "Assignment") {
          if (token.value === "to") reachedTo = true;
        }
        else {
          if (token.type === "Identifier") {
            newData.push(getVar(token.value.toString(), state));
          }
          else if (token.type === "String") {
            newData.push(getStringData(token, state));
          }
          else if (token.type === "Number") {
            newData.push(getNumberData(token, state));
          }
        }
      }
    }
    if (reachedTo) {
      if (tokens[tokens.length - 1].type === "Identifier") {
        const arr = getVar(tokens[tokens.length - 1].value.toString(), state);
        if (arr instanceof Array) {
          setVar(tokens[tokens.length - 1].value.toString(), arr.concat(newData), state);
        }
      }
    }
  },
  async pop(state: State, from: Token, array: Token, to: Token, variable: Token) {
    if (from.type === "Assignment" && from.value === "from") {
      if (array.type === "Identifier") {
        const arr = getVar(array.value.toString(), state);
        if (arr instanceof Array) {
          if (to.type === "Assignment" && to.value === "to") {
            if (variable.type === "Identifier") {
              const val = arr.pop();
              setVar(variable.value.toString(), val, state);
            }
          }
        }
      }
    }
  },
  async export(state: State, namespace: Token) {
    state.exports[namespace.value] = state.namespaces[namespace.value];
  },
  async import(state: State, namespace: Token, from: Token, file: Token) {
    return new Promise((resolve, reject) => {
      if (file.type === "String") {
        const code = fs.readFileSync(file.value.toString(), 'utf-8');
        if (code) {
          const interp = new Interpreter({module: true});
          interp.run(code).then((namespaces: Namespaces | undefined) => {
            if (namespaces) {
              for (let namespace in namespaces) {
                if (namespaces.hasOwnProperty(namespace)) {
                  state.namespaces[namespace] = namespaces[namespace];
                }
              }
              resolve();
            }
          })
        }
      }
    })
  }
}
